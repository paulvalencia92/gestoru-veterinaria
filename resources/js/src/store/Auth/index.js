import axios from "axios";
import {core} from "../../config/pluginInit";

export default {
    namespaced: true,
    state: {
        access_token: null,
        user: null,
        abilities: [],
    },
    getters: {},
    mutations: {
        SET_USER_DATA(state, data) {
            state.access_token = data.access_token;
            state.user = data.user;
            state.abilities = data.abilities;
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.access_token;
        },
        CLEAR_USER_DATA(state, data) {
            state.access_token = null;
            state.user = null;
        },
    },
    actions: {
        async login(context, userData) {
            let data = {
                email: userData.email,
                password: userData.password
            }
            const response = await axios.post('/api/login', data);
            if (response.status == 200) {
                context.commit('SET_USER_DATA', response.data);
                return Promise.resolve(response.status);
            } else if (response.data.errors.length > 0) {
                core.showSnackbar("error", "El usuario y la contraseña no coinciden");
                context.commit('SET_ERRORS', response.data.errors, {root: true});
            }





            // const response = await axios.post('/api/login', data).then(response => {
            //     if (response.status == 200) {
            //         context.commit('SET_USER_DATA', response.data);
            //         return Promise.resolve(200);
            //     } else if (response.data.errors.length > 0) {
            //         core.showSnackbar("error", "El usuario y la contraseña no coinciden");
            //         context.commit('SET_ERRORS', response.data.errors, {root: true});
            //     }
            // }).finally(() => {
            //     this.loading = false;
            // });

        },
        async logout(context) {
            const response = await axios.post('/api/logout-user');
            console.log(response)
            context.commit('CLEAR_USER_DATA');
        },

    },
};
