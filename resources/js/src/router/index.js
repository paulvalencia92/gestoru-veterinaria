import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './../store/index'
import axios from "axios";


/* Layouts */
const VerticleLayout = () => import('../layouts/VerticleLayout')
const Default = () => import('../layouts/BlankLayout')
const AuthLayout = () => import('../layouts/AuthLayouts/AuthLayout')

/* Dashboards View */
const Dashboard = () => import('../views/Dashboards/Dashboard.vue')

/* Authentic View */
const SignIn1 = () => import('../views/AuthPages/Default/SignIn1')
const SignUp1 = () => import('../views/AuthPages/Default/SignUp1')
const RecoverPassword1 = () => import('../views/AuthPages/Default/RecoverPassword1')
const LockScreen1 = () => import('../views/AuthPages/Default/LockScreen1')
const ConfirmMail1 = () => import('../views/AuthPages/Default/ConfirmMail1')
const Callback = () => import('../views/AuthPages/Default/Callback')

/* Extra Pages */
const ErrorPage = () => import('../views/Pages/ErrorPage')
const Maintenance = () => import('../views/Pages/Maintenance')
const BlankPage = () => import('../views/Pages/BlankPage')

const RoleApp = () => import(/* webpackChunkName:"role-app"*/ "../views/Config/Role/RoleApp");


const ListPets = () => import(/* webpackChunkName:"list-pets"*/ '../views/Pet/ListPet');
const ListPetType = () => import(/* webpackChunkName:"list-pets-types"*/ '../views/PetType/ListPetType');


Vue.use(VueRouter)

const childRoutes = (prop) => [
    {
        path: 'home',
        name: prop + '.home',
        meta: {auth: true, name: 'Home'},
        component: Dashboard
    },
    {
        path: 'tipos-de-mascotas',
        name: prop + '.pet-types',
        meta: {auth: true, name: 'Tipos de Mascotas'},
        component: ListPetType
    },
    {
        path: 'mascotas',
        name: prop + '.pets',
        meta: {auth: true, name: 'Mascotas'},
        component: ListPets
    },
]

const authChildRoutes = (prop) => [
    {
        path: 'sign-in1',
        name: prop + '.sign-in1',
        meta: {auth: false},
        component: SignIn1
    },
    {
        path: 'sign-up1',
        name: prop + '.sign-up1',
        meta: {auth: true},
        component: SignUp1
    },
    {
        path: 'password-reset1',
        name: prop + '.password-reset1',
        meta: {auth: false},
        component: RecoverPassword1
    },
]

const defaultlayout = (prop) => [
    {
        path: 'blank-page',
        name: prop + '.blank-page',
        meta: {auth: true, name: 'Blank Page'},
        component: BlankPage
    },
]

const pagesChildRoutes = (prop) => [
    {
        path: 'error/:code',
        name: prop + '.error',
        meta: {auth: true},
        component: ErrorPage
    },

    {
        path: 'maintenance',
        name: prop + '.maintenance',
        meta: {auth: true},
        component: Maintenance
    }
]


//*-------------------------------//
//-----Config child routes        //
//-------------------------------//

const configChildRoute = (prop) => [
    {
        path: '/role',
        name: prop + '.role',
        meta: {auth: true, name: 'Roles'},
        component: RoleApp
    },
]


//*-------------------------------//
//-----------RUTAS GENERALES      //
//-------------------------------//

const routes = [
    {
        path: '/',
        name: 'dashboard',
        component: VerticleLayout,
        meta: {auth: true},
        children: childRoutes('app')
    },
    {
        path: '/auth',
        name: 'auth1',
        component: AuthLayout,
        meta: {auth: true},
        children: authChildRoutes('auth1')
    },
    {
        path: '/pages',
        name: 'pages',
        component: Default,
        meta: {auth: true},
        children: pagesChildRoutes('default')
    },
    {
        path: '/extra-pages',
        name: 'extra-pages',
        component: VerticleLayout,
        meta: {auth: true},
        children: defaultlayout('extra-pages')
    },
    {
        path: '/config',
        name: 'config',
        component: VerticleLayout,
        meta: {auth: true},
        children: configChildRoute('config')
    },

    {
        path: '/callback',
        name: 'callback',
        meta: {auth: false},
        component: Callback
    }
]


// axios.interceptors.response.use(response => {
//     // console.log(response)
//     if (response.data.status == false) {
//         store.commit('Auth/CLEAR_USER_DATA');
//     }
//     return response
// });


const router = new VueRouter({
    mode: 'history',
    base: process.env.MIX_SENTRY_DSN_INDEX,
    routes
})
router.beforeEach((to, from, next) => {

    store.commit('CLEAR_ERRORS');

    const publicPages = ['/auth/sign-in1', '/auth/sign-up1', '/dark/auth/sign-in1', '/dark/auth/sign-up1']
    const authRequired = !publicPages.includes(to.path)
    const loggedIn = store.state.Auth.user


    if (to.meta.auth) {
        if (authRequired && loggedIn === null) {
            return next('/auth/sign-in1')
        } else if (to.name === 'dashboard' || to.name === 'mini.dashboard') {
            return next('/home')
        }
    } else {
        if (loggedIn) {
            return next('/home')
        } else {
            next()
        }
    }
    next()
})

export default router
