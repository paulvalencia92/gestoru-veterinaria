import axios from "axios";

export default {
    namespaced: true,
    state: {
        pets: [],
    },
    getters: {},
    mutations: {
        SET_PETS(state, pets) {
            state.pets = pets;
        },
        ADD_PET(state, pet) {
            state.pets.push(pet);
        },
        UPDATE_PET(state, petUpdated) {
            const index = state.pets.findIndex(pet => pet.id == petUpdated.id);
            state.pets.splice(index, 1, petUpdated);
        },
        DELETE_PET(state, petId) {
            const index = state.pets.findIndex(pet => pet.id == petId);
            state.pets.splice(index, 1);
        },
    },
    actions: {
        async getPets(context) {
            const response = await axios.get('/api/pets');
            context.commit('SET_PETS', response.data);
        },
        async createPet(context, payload) {
            const response = await axios.post('/api/pets', payload);
            if (response.data.errors) {
                context.commit('SET_ERRORS', response.data.errors, {root: true});
                core.showSnackbar("error", "No ha podido registrar la mascota");
                return Promise.reject('error');
            }
            context.commit('ADD_PET', response.data);
            return Promise.resolve('Success');
        },
        async UpdatePet(context, payload) {
            const response = await axios.put(`/api/pets/${payload.id}`, payload);
            if (response.data.errors) {
                context.commit('SET_ERRORS', response.data.errors, {root: true});
                core.showSnackbar("error", "No ha podido actualizar la mascota");
                return Promise.reject('error');
            }
            context.commit('UPDATE_PET', response.data);
            return Promise.resolve('Success');
        },
        async deletePet(context, petId) {
            await axios.delete(`/api/pets/${petId}`);
            context.commit('DELETE_PET', petId);
            return Promise.resolve('Delete success');
        }

    },
};
