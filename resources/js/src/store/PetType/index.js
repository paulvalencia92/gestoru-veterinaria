import axios from "axios";

export default {
    namespaced: true,
    state: {
        petTypes: [],
    },
    getters: {
        optionsPetTypes(state) {
            return state.petTypes.map(item => {
                return {text: item.name, value: item.id}
            })
        }
    },
    mutations: {
        SET_PET_TYPES(state, petTypes) {
            state.petTypes = petTypes;
        },
        ADD_PET_TYPE(state, petType) {
            state.petTypes.push(petType);
        },
        UPDATE_PET_TYPE(state, petTypeUpdated) {
            const index = state.petTypes.findIndex(petType => petType.id == petTypeUpdated.id);
            state.petTypes.splice(index, 1, petTypeUpdated);
        },
        DELETE_PET_TYPE(state, petTypeId) {
            const index = state.petTypes.findIndex(petType => petType.id == petTypeId);
            state.petTypes.splice(index, 1);
        },

    },
    actions: {
        async getPetTypes(context) {
            const response = await axios.get('/api/pet-types');
            context.commit('SET_PET_TYPES', response.data);
        },
        async createPetType(context, payload) {
            const response = await axios.post('/api/pet-types', payload);
            if (response.data.errors) {
                context.commit('SET_ERRORS', response.data.errors, {root: true});
                core.showSnackbar("error", "No se ha podido registrar el tipo de mascota");
                return Promise.reject('error');
            }
            context.commit('ADD_PET_TYPE', response.data);
            return Promise.resolve('Success');
        },
        async updatePetType(context, petType) {
            const response = await axios.put(`/api/pet-types/${petType.id}`, petType);
            if (response.data.errors) {
                context.commit('SET_ERRORS', response.data.errors, {root: true});
                core.showSnackbar("error", "No se ha podido actualizar el tipo de mascota");
                return Promise.reject('error');
            }
            context.commit('UPDATE_PET_TYPE', response.data);
            return Promise.resolve('Success');
        },
        async deletePetType(context, petTypeId) {
            const response = await axios.delete(`/api/pet-types/${petTypeId}`);
            context.commit('DELETE_PET_TYPE', response.data);
            return Promise.resolve('Success');
        },

    },
};
