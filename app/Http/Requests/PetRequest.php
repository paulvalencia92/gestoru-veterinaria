<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'POST':
            {
                return [
                    'name' => ['required'],
                    'pet_type_id' => ['required', 'exists:App\Models\PetType,id'],
                    'breed' => ['required'],
                    'date_of_birth' => ['required', 'date'],

                    'owner_name' => ['required'],
                    'phone_number' => ['required'],
                    'address' => ['required'],
                    'email' => ['required', Rule::unique('owners')],
                ];
            }
            case 'PUT':
            {
                return [
                    'name' => ['required'],
                    'pet_type_id' => ['required', 'exists:App\Models\PetType,id'],
                    'breed' => ['required'],
                    'date_of_birth' => ['required', 'date'],

                    'owner_name' => ['required'],
                    'phone_number' => ['required'],
                    'address' => ['required'],
                    'email' => ['required', Rule::unique('owners')->ignore($this->pet->owner_id)]
                ];
            }
        }


    }
}
