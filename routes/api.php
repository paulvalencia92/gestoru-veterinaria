<?php

use App\Http\Controllers\{AdvertisementController, AuthController, RoleController, RolesController, UnitController};
use Illuminate\Support\Facades\Route;

Auth::routes();


Route::group(['middleware' => 'auth:api'], function () {

//    Route::get('/roles', [RolesController::class, 'index']);


    /**=================================
     *    Tipos de mascotas
     *================================**/
    Route::apiResource('/pet-types', 'PetTypeController');

    /**=================================
     *    Mascotas
     *================================**/
    Route::apiResource('/pets', 'PetController');


    Route::post('/logout-user', [AuthController::class, 'logout']);


});

Route::post('/login', [AuthController::class, 'login']);
