<?php

namespace App\Http\Controllers;

use App\Models\PetType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PetTypeController extends Controller
{

    public function index()
    {
        $petTypes = PetType::all();
        return response()->json($petTypes, 200);
    }


    public function store(Request $request)
    {
        $dataValidated = $request->validate([
            'name' => ['required', Rule::unique('pet_types')],
        ]);

        $petType = PetType::create([
            'name' => $dataValidated['name']
        ]);

        return response()->json($petType, 201);
    }


    public function update(Request $request, PetType $petType)
    {
        $dataValidated = $request->validate([
            'name' => ['required', Rule::unique('pet_types')->ignore($petType->id)],
        ]);

        $petType->name = $dataValidated['name'];
        $petType->update();

        return response()->json($petType, 201);

    }


    public function destroy(PetType $petType)
    {
        $petType->delete();
        return response()->json('delete success');
    }
}
