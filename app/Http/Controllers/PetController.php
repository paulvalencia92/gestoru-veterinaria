<?php

namespace App\Http\Controllers;

use App\Http\Requests\PetRequest;
use App\Models\Owner;
use App\Models\Pet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PetController extends Controller
{

    public function index()
    {
        $pets = Pet::with('owner', 'pet_type')->get();
        return response()->json($pets, 201);
    }


    public function store(PetRequest $request)
    {
        DB::beginTransaction();

        $owner = Owner::create([
            'name' => $request->owner_name,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
            'email' => $request->email,
        ]);

        $pet = $owner->pets()->create([
            'name' => $request->name,
            'pet_type_id' => $request->pet_type_id,
            'breed' => $request->breed,
            'date_of_birth' => $request->date_of_birth,
        ]);


        DB::commit();
        return response()->json($pet->load('owner', 'pet_type'), 201);
    }


    public function update(PetRequest $request, Pet $pet)
    {

        $pet->name = $request->name;
        $pet->pet_type_id = $request->pet_type_id;
        $pet->breed = $request->breed;
        $pet->date_of_birth = $request->date_of_birth;
        $pet->update();

        $owner = Owner::find($pet->owner_id);
        $owner->name = $request->owner_name;
        $owner->phone_number = $request->phone_number;
        $owner->address = $request->address;
        $owner->email = $request->email;
        $owner->update();
        return response()->json($pet->load('owner', 'pet_type'), 201);
    }

    public function destroy(Pet $pet)
    {
        $pet->delete();
        return response()->json('Pet deleted', 200);
    }
}
