<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    protected $fillable = ['name', 'pet_type_id', 'breed', 'date_of_birth', 'image'];

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }

    public function pet_type()
    {
        return $this->belongsTo(PetType::class);
    }
}
