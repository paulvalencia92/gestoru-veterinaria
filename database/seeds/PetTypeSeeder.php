<?php

use App\Models\PetType;
use Illuminate\Database\Seeder;

class PetTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PetType::create([
            'name' => 'Perro',
        ]);

        PetType::create([
            'name' => 'Gato',
        ]);

        PetType::create([
            'name' => 'Loro',
        ]);
    }
}
